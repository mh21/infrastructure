---
stages:
  - 🎬  # lint preparations
  - 🐛  # linting
  - 📦  # tagging
  - 🚀  # yamls
  - ⛅  # playbooks
  - 🚨  # interactive actions

default:
  tags:
    - deployment-runner

.common_setup: &common_setup |
  # set -eo pipefail is already set by gitlab-runner
  set -u
  # Make getpwuid() happy
  echo "cki:x:`id -u`:`id -g`:,,,:${HOME}:/bin/bash" >> /etc/passwd
  mkdir ~/.ssh
  echo -e "Host *\nStrictHostKeyChecking no" > ~/.ssh/config
  chmod 600 ~/.ssh/config
  # display external IP
  echo "External IP: $(curl ipv4.icanhazip.com)"

.schedule:
  rules:
    # if a matching job has been triggered via the api/schedule
    - if: $CI_PIPELINE_SOURCE =~ /api|schedule/ && $ONLY_PROJECT_NAME && $PROJECT_NAME && $ONLY_PROJECT_NAME == $PROJECT_NAME
      when: on_success
    - if: $CI_PIPELINE_SOURCE =~ /api|schedule/ && $ONLY_PROJECT_NAME && $PROJECT_GROUP && $ONLY_PROJECT_NAME == $PROJECT_GROUP
      when: on_success
    - if: $CI_PIPELINE_SOURCE =~ /api|schedule/ && $ONLY_PROJECT_NAME && $PROJECT_REGEX && $ONLY_PROJECT_NAME =~ $PROJECT_REGEX
      when: on_success
    - if: $CI_PIPELINE_SOURCE =~ /api|schedule/ && $ONLY_PROJECT_NAME && $PLAYBOOK_DIR && $ONLY_PROJECT_NAME == $PLAYBOOK_DIR
      when: on_success
    # if a non-matching job has been triggered via the api/schedule
    - if: $CI_PIPELINE_SOURCE =~ /api|schedule/
      when: never

.common:
  extends: .cki_tools
  variables:
    ANSIBLE_HOST_KEY_CHECKING: 'false'
  before_script:
    - *common_setup
  rules:
    - !reference [.schedule, rules]
    - when: manual
      allow_failure: true

.auto:
  rules:
    - !reference [.schedule, rules]
    # do not run anything automatically for new tags, e.g. when the deployment-bot tag changes
    - if: $CI_COMMIT_TAG
      when: manual
      allow_failure: true
    # for the tip of the default branch
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success
      allow_failure: true
    - when: manual
      allow_failure: true

.beaker_update:
  extends: .common
  stage: ⛅
  script:
    - ./beaker_deploy.sh ${PLAYBOOK_HOST+--limit "$PLAYBOOK_HOST"}

.kubernetes:
  extends: .common
  before_script:
    - *common_setup

.kubernetes_deploy:
  extends: .kubernetes
  stage: 🚀
  script:
    - ./kubernetes_deploy.sh
  artifacts:
    paths:
      # this needs to be a simple artifact (no report) to allow the
      # deployment-bot to download it via the API
      - deploy.env

.kubernetes_ansible_config:
  extends: .kubernetes
  stage: ⛅
  script:
    - ${PLAYBOOK_DIR}/deploy.sh

.lint:
  stage: 🐛
  rules:
    # never for api-triggered pipelines
    - if: $CI_PIPELINE_SOURCE =~ /api|schedule/
      when: never
    - when: on_success

.kubernetes_lint:
  extends: [.kubernetes, .lint]
  script:
    - ./lint.sh "$LINT"

.aws:
  extends: .common
  before_script:
    - *common_setup

.aws_deploy:
  extends: .aws
  stage: ⛅
  script:
    - ./aws_deploy.sh ${PLAYBOOK_HOST+--limit "$PLAYBOOK_HOST"}
